import os
from PIL import Image, ImageDraw, ImageFont, ImageEnhance
import random
import math
import re

if __name__ == "__main__":
	dim = 300
	path = './Images' 
	box_path = './box_images' 

	square_path = './SquareImages' 
	for folder in os.listdir(path):
		for file in os.listdir(path	+ '/' + folder + '/'):
			if '.DS' in file: continue
			image_path = path + '/' + folder + '/' + file
			
			image = Image.open(image_path)
			image.thumbnail((dim,dim), Image.ANTIALIAS)

			x = int((dim - image.size[0])/2)
			y = int((dim - image.size[1])/2)
			white_background = Image.new('RGB', (dim,dim), (255, 255, 255))
			white_background.paste(image, (x,y))

			new_folder_name = 'distractors' if folder == 'distractors' else file.split('_')[0][:-1]
			target_folder = square_path + '/' + new_folder_name

			if not os.path.exists(target_folder): os.makedirs(target_folder)
			white_background.save(target_folder + '/' + file.split('.')[0] + '.png')

	## Get distractors from box_images
	for folder in os.listdir(box_path):
		if '.DS' in folder: continue
		for file in os.listdir(box_path	+ '/' + folder + '/'):
			if '.DS' in file: continue
			if '1' in file or '2' in file:
				image_path = box_path + '/' + folder + '/' + file
				image = Image.open(image_path)
				image.thumbnail((dim,dim), Image.ANTIALIAS)

				x = int((dim - image.size[0])/2)
				y = int((dim - image.size[1])/2)
				white_background = Image.new('RGB', (dim,dim), (255, 255, 255))
				white_background.paste(image, (x,y))
				
				new_folder_name = 'distractors'
				target_folder = square_path + '/' + new_folder_name

				if not os.path.exists(target_folder): os.makedirs(target_folder)
				white_background.save(target_folder + '/' + file.split('.')[0].lower() + '.png')

