import os
import math

cwd = os.getcwd()

	
# Merge event and gaze data for each subject
# For each subject
eventheader = ''
eyegazeheader = ''
combinedLines = []

for ID in range(1,49):
	eventcsv = cwd+'/subj-'+str(ID)+'eventData.csv'
	eyegazecsv = cwd+'/subj-'+str(ID)+'eyeGazeData.csv'
	if (not os.path.exists(eventcsv)) or (not os.path.exists(eyegazecsv)): continue	
	with open(eventcsv, 'r') as eventf:
		with open(eyegazecsv, 'r') as eyegazef:
			# Get lines and header
			eventlines = eventf.readlines()
			eyegazelines = eyegazef.readlines()
			eventheader = eventlines[0].strip()
			eyegazeheader = eyegazelines[0].strip()
			# Make dictionary of data entry to line
			dataEntry2Line = {}
			for i in range(1,len(eventlines)):
				line = eventlines[i].strip()
				eventDataEntry = int(line.split(',')[2])
				dataEntry2Line[eventDataEntry] = line

			# Append the combined lines
			for i in range(1,len(eyegazelines)):
				line = eyegazelines[i].strip()
				eyeDataEntry = int(line.split(',')[1])
				combinedLines.append(line + ',' + dataEntry2Line[eyeDataEntry])
header = eyegazeheader+','+eventheader

# Remove extra columns; data entry (10) & ID (12):
for i in range(len(combinedLines)):
	line = combinedLines[i]
	cols = line.split(',')
	del cols[12]
	del cols[10]
	combinedLines[i] = ','.join(cols)
header_cols = header.split(',')
del header_cols[12]
del header_cols[10]
header_cols.extend(['Block','Competitor','TargetPos','CompetitorPos','ReferenceAOI','TargetAOI','CompetitorAOI','UnrelatedAOI','Out','Correct','Item'])
header = ','.join(header_cols)

# Expand Slide info - block, competitor, targetpos, competitorpos
for i in range(len(combinedLines)):
	line = combinedLines[i]
	cols = line.split(',')
	slide = cols[-3] # ..., slide, xclick, yclick.
	s = slide.split('_')
	cols.extend([s[4][-1],str(1 if 'p'==s[6] else 0),s[7][-1],s[8][2]])
	combinedLines[i] = ','.join(cols)

###############
# Mark AOI Info
# AOIs: 0-6 position index in hexagonal layout - each col of hex: 0,1; 2,3,4; 5,6
dim = 250
gap = int((1080 - 3*dim)/3)
w = 1920
h = 1080
yw = 1080/3
xw = 1920/3
margin = 25

aois = {
		0: (int((w-3*dim-2*gap)/2)-margin,int((w-3*dim-2*gap)/2)+margin+dim,int((1080-2*dim-gap)/2)-margin,int((1080-2*dim-gap)/2)+margin+dim),
		1: (int((w-3*dim-2*gap)/2)-margin,int((w-3*dim-2*gap)/2)+margin+dim,int((1080-2*dim-gap)/2)+dim+gap-margin,int((1080-2*dim-gap)/2)+dim+gap+margin+dim),
		2: (640+int((640-dim)/2)-margin,640+int((640-dim)/2)+margin+dim,int(0*360+(360-dim)/2)-margin,int(0*360+(360-dim)/2)+margin+dim),
		3: (640+int((640-dim)/2)-margin,640+int((640-dim)/2)+margin+dim,int(1*360+(360-dim)/2)-margin,int(1*360+(360-dim)/2)+margin+dim),
		4: (640+int((640-dim)/2)-margin,640+int((640-dim)/2)+margin+dim,int(2*360+(360-dim)/2)-margin,int(2*360+(360-dim)/2)+margin+dim),
		5: (int((w-3*dim-2*gap)/2)+2*(gap+dim)-margin,int((w-3*dim-2*gap)/2)+2*(gap+dim)+margin+dim,int((1080-2*dim-gap)/2)-margin,int((1080-2*dim-gap)/2)+margin+dim),
		6: (int((w-3*dim-2*gap)/2)+2*(gap+dim)-margin,int((w-3*dim-2*gap)/2)+2*(gap+dim)+margin+dim,int((1080-2*dim-gap)/2)+dim+gap-margin,int((1080-2*dim-gap)/2)+dim+gap+margin+dim)
		}
# aois = {i:(int((i%3)*xw+margin),int((i%3)*xw+xw-margin),
# 	int((math.floor(i/3)*yw+margin)),int((math.floor(i/3)*yw+yw-margin))) for i in range(9)}
# print(aois)

for i in range(len(combinedLines)):
	line = combinedLines[i]
	cols = line.split(',')
	
	lX,lY,rX,rY,validityLeft,validityRight = (int(round(float(item))) for item in cols[4:10])
	# Get gaze point
	gazeX, gazeY = 0,0
	if validityLeft == 0 and validityRight == 0: gazeX = (lX+rX)/2; gazeY = (lY+rY)/2 # both eyes good
	elif validityLeft == 0 and validityRight == 4: gazeX = lX; gazeY = lY # probably left good
	elif validityLeft == 1 and validityRight == 3: gazeX = lX; gazeY = lY # probably left good
	elif validityLeft == 2 and validityRight == 2: gazeX = -1; gazeY = -1 # don't know which iye
	elif validityLeft == 3 and validityRight == 1: gazeX = rX; gazeY = rY # probably right good
	elif validityLeft == 4 and validityRight == 0: gazeX = rX; gazeY = rY # probably right good
	elif validityLeft == 4 and validityRight == 4: gazeX = -1; gazeY = -1 # no eyes found

	referenceAOI, targetAOI,competitorAOI,unrelatedAOI,out,correct = 0,0,0,0,0,0
	comp_pres,tarpos,comppos,refpos = (int(item) for item in cols[17:20]+[3])
	# Decide which AOI was looked at
	if gazeX >= 0 or gazeY >= 0:
		# check if looked at reference
		if (aois[refpos][0] < gazeX < aois[refpos][1]) and (aois[refpos][2] < gazeY < aois[refpos][3]): 
			referenceAOI = 1
		elif (aois[tarpos][0] < gazeX < aois[tarpos][1]) and (aois[tarpos][2] < gazeY < aois[tarpos][3]): 
			targetAOI = 1
		elif (aois[comppos][0] < gazeX < aois[comppos][1]) and (aois[comppos][2] < gazeY < aois[comppos][3]): 
			competitorAOI = 1
		elif any((aois[pos][0] < gazeX < aois[pos][1]) and (aois[pos][2] < gazeY < aois[pos][3]) for pos in aois.keys()):
			unrelatedAOI = 1
	else:
		out = 1

	# check if correct target is selected
	xClick, yClick = int(cols[14]), int(cols[15])
	if (aois[tarpos][0] < xClick < aois[tarpos][1]) and (aois[tarpos][2] < yClick < aois[tarpos][3]): 
		correct = 1

	cols.extend([str(item) for item in [referenceAOI,targetAOI,competitorAOI,unrelatedAOI,out,correct]])
	combinedLines[i] = ','.join(cols)

# Get item
for i in range(len(combinedLines)):
	line = combinedLines[i]
	cols = line.split(',')
	slide = cols[13]
	s = slide.split('_')
	cols.extend([s[2]])
	combinedLines[i] = ','.join(cols)

# Write to output file
with open('combined.csv','w') as out:
	out.write(header + '\n')
	for line in combinedLines:
		out.write(line+'\n')

print('Combined output CSV files.')
