set.seed(42)

library("Matrix")
library("lme4")
library("ggplot2")
library("eyetrackingR")
library("tidyr")
library(plyr) #ddply
library(stringr)

# LOAD EXP2
setwd("~/Desktop/Stuff/Penn/Research/Lemmas/Experiment2/analysis/")
raw_data <- read.table(file = "./combined_truncated.csv", sep = ',', header = TRUE)
raw_fix = raw_data
raw_fix$ID = seq.int(nrow(raw_data))
raw_fix$Slide = paste(raw_fix$SubjectID,raw_fix$Slide)
eyetracking_data = make_eyetrackingr_data(raw_fix, participant_column="SubjectID", trackloss_column="Out", time_column="ID",
                                          trial_column="Slide", aoi_columns=c("ReferenceAOI","TargetAOI","CompetitorAOI","UnrelatedAOI"), treat_non_aoi_looks_as_missing=FALSE)
trackloss =trackloss_analysis(eyetracking_data)

good_slides = subset(trackloss, (TracklossForTrial<0.25))$Slide
# good_slides = subset(trackloss, (TracklossForParticipant<0.25 & TracklossForTrial<0.25))$Slide
raw_data_filtered = subset(raw_fix, is.element(Slide, good_slides))
unique(raw_data_filtered$SubjectID)
data <- raw_data_filtered[,c("SubjectID","DataEntryNum","timestampLocal",
                             "List","Task","Block","Slide","Item","Competitor",
                             "ReferenceAOI","TargetAOI","CompetitorAOI",
                             "UnrelatedAOI","Out","Correct","compAfterTarget")]
#Remove irrelevant stimuli (remove instructions; filter homophone-present)
data = subset(data, Correct==1)
data = subset(data, SubjectID != 1)  
# data = subset(data, DataEntryNum <= 24)
homos <- c('bat','bow','flower','horn','nail','nut','pitcher','tank')
data = subset(data, is.element(Item, homos))
exp2_data <- data[order(data[,1], data[,2], data[,3]),]

# LOAD EXP1
setwd("~/Desktop/Stuff/Penn/Research/Lemmas/analysis/")
raw_data <- read.table(file = "./combined_truncated.csv", sep = ',', header = TRUE)
raw_fix = raw_data
raw_fix$ID = seq.int(nrow(raw_data))
raw_fix$Slide = paste(raw_fix$SubjectID,raw_fix$Slide)
eyetracking_data = make_eyetrackingr_data(raw_fix, participant_column="SubjectID", trackloss_column="Out", time_column="ID",
                                          trial_column="Slide", aoi_columns=c("ReferenceAOI","TargetAOI","CompetitorAOI","UnrelatedAOI"), treat_non_aoi_looks_as_missing=FALSE)
trackloss =trackloss_analysis(eyetracking_data)

good_slides = subset(trackloss, (TracklossForTrial<0.25))$Slide
# good_slides = subset(trackloss, (TracklossForParticipant<0.25 & TracklossForTrial<0.25))$Slide
raw_data_filtered = subset(raw_fix, is.element(Slide, good_slides))
unique(raw_data_filtered$SubjectID)
data <- raw_data_filtered[,c("SubjectID","DataEntryNum","timestampLocal",
                             "List","Task","Block","Slide","Item","Competitor",
                             "ReferenceAOI","TargetAOI","CompetitorAOI",
                             "UnrelatedAOI","Out","Correct","compAfterTarget")]
#Remove irrelevant stimuli (remove instructions; filter homophone-present)
data = subset(data, Correct==1)
homos <- c('bat','bow','flower','horn','nail','nut','pitcher','tank')
data = subset(data, is.element(Item, homos))
exp1_data <- data[order(data[,1], data[,2], data[,3]),]



############## RT ANALYSIS

rt_subject_1 = 
  exp1_data[,c('Task','SubjectID','Block','Item','Competitor','Correct')] %>%
  ddply(.(Task, SubjectID, Item, Block, Competitor), numcolwise(sum)) %>%
  mutate(Block = ifelse(Block == 2 | Block == 1, 1,
                        ifelse(Block == 4 | Block == 3, 2, NA))) %>%
  unite(SubjectID, SubjectID, Block)  %>%
  ddply(.(Task, SubjectID, Competitor), numcolwise(mean))
rt_subject_1 = rt_subject_1[,c('Task','SubjectID','Competitor','Correct')]
rt_subject_1$exp = 1

rt_subject_2 = 
  exp2_data[,c('Task','SubjectID','Block','Item','Competitor','Correct')] %>%
  ddply(.(Task, SubjectID, Item, Block, Competitor), numcolwise(sum)) %>%
  mutate(Block = ifelse(Block == 2 | Block == 1, 1,
                        ifelse(Block == 4 | Block == 3, 2, NA))) %>%
  unite(SubjectID, SubjectID, Block)  %>%
  ddply(.(Task, SubjectID, Competitor), numcolwise(mean))
rt_subject_2 = rt_subject_2[,c('Task','SubjectID','Competitor','Correct')]
rt_subject_2$exp = 2
rt_subject = rbind(rt_subject_1, rt_subject_2)
rt_subject = mutate(rt_subject, Correct = Correct *1000/120)

rt = rt_subject
rt = subset(rt, Competitor == 1)
rt = subset(rt, Task == 'Semantic')
t.test(rt$Correct ~ rt$exp, mu=0, conf=0.95, var.eq=F, paired=F)

library(lme4)
fm0 <- lmer(Correct~(1|exp), data = rt)
fm1 <- lmer(Correct~(1|exp), data = rt)
# fm1 <- lmer(rt_subject_2$Correct~(1|id), data = dataset)
# fm1 <- lmer(value~fac1+(1|idconst), data = dataset)

anova(fm1,fm0)

rt_results_subject = spread(rt_subject, Competitor, Correct)
names(rt_results_subject) <- c("Task", "SubjectID", "unrelated", "homophone")

#Generate final data ready to plot
final <- ddply(rt_results_subject, c("Task"), summarise,
               N_h    = sum(!is.na(homophone)),
               N_u    = sum(!is.na(unrelated)),
               mean_h = mean(homophone, na.rm=TRUE)*1000/120,
               mean_u = mean(unrelated, na.rm=TRUE)*1000/120,
               sd_h   = sd(homophone, na.rm=TRUE)*1000/120,
               sd_u   = sd(unrelated, na.rm=TRUE)*1000/120,
               se_h   = sd_h / sqrt(N_h),
               se_u   = sd_u / sqrt(N_u))

#Reformat for plotting
plotdat_homo <- final[,c('Task','mean_h','sd_h','se_h')]
plotdat_homo$type<-'Homophone Present'
colnames(plotdat_homo)[2]<-'Proportion'
colnames(plotdat_homo)[3]<-'SD'
colnames(plotdat_homo)[4]<-'SE'
plotdat_ur <- final[,c('Task','mean_u','sd_u','se_u')]
plotdat_ur$type<-'Unrelated Absent'
colnames(plotdat_ur)[2]<-'Proportion'
colnames(plotdat_ur)[3]<-'SD'
colnames(plotdat_ur)[4]<-'SE'
plotdat <-rbind(plotdat_ur, plotdat_homo)
#Plot
proportion_plot <- ggplot(data=plotdat, aes(x=Task, y=Proportion, fill=type)) +
  geom_bar(stat="identity", position=position_dodge()) +
  geom_errorbar(aes(ymin=Proportion-SE, ymax=Proportion+SE),
                width=.2,                    # Width of the error bars
                position=position_dodge(.9))

print(proportion_plot +  labs(title= "Response time across tasks by subjects",
                              y="Number of samples", x = "Task", fill = "Image Type"))




