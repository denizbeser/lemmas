import os
from PIL import Image, ImageDraw, ImageFont, ImageEnhance
import random
import math
import re

dim = 250
random.seed(1)

def combine_images(ids, images, reference):
	# images[0] is target; images[1] is competitor
	image2ids = zip(images, ids)
	background = Image.new('RGBA', (1920,1080), (255, 255, 255))
	target_pos = None
	competitor_pos = None
	for image, ID in image2ids:
		if image == images[0]: target_pos = ID
		elif image == images[1]: competitor_pos = ID
		image = image.resize((dim,dim), Image.ANTIALIAS)
		background.paste(image.convert('RGBA'), id2pos[ID], image.convert('RGBA'))
		reference = reference.resize((dim,dim), Image.ANTIALIAS)
	background.paste(reference.convert('RGBA'), id2pos[3], reference.convert('RGBA'))
	# background.show()
	# return image, target pos and competitor pos
	return background, str(target_pos), str(competitor_pos)

def generate_task(task):
	# Generate Random List 1 and List 2 homophones:
	homo_folders = [folder for folder in os.listdir(path) if folder != 'distractors' and '.DS' not in folder]
	# Decide which blocks will have which homos with competitor present
	b1_homos = homo_folders[:4]
	b2_homos = homo_folders[4:]
	b3_homos = homo_folders[:4]
	b4_homos = homo_folders[4:]
	b1_homo_slides = []
	b2_homo_slides = []
	b3_homo_slides = []
	b4_homo_slides = []

	# Make 8 homophone slides
	homo_slides = []
	for folder in homo_folders:
		if '.DS' in folder: continue
		homo_file_1 = folder+'1.png'
		homo_file_2 = folder+'2.png'
		homo_file_3 = folder+'3.png'
		homo_file_4 = folder+'4.png'
		# reference at id 4; randomly placed target; 1 homo; rest is random distractors
		# 3 by 3, center one in each 640/360 rectangle, random order
		homo0_1 = Image.open(path+'/'+folder+'/'+homo_file_1)
		homo0_2 = Image.open(path+'/'+folder+'/'+ (homo_file_2 if task != 'Exact' else homo_file_1))
		homo1_1 = Image.open(path+'/'+folder+'/'+homo_file_3)
		homo1_2 = Image.open(path+'/'+folder+'/'+(homo_file_4 if task != 'Exact' else homo_file_3))
		
		# Setup the other images in the grid:
		images_b1 = random.sample(distractors_1, 6)
		images_b2 = [i for i in images_b1]
		images_b3 = random.sample(distractors_2, 6)
		images_b4 = [i for i in images_b3]
		# Coutnerbalancing target homophone in blocks 1vs4 2vs3
		images_b1[0] = homo0_1 # target image
		images_b2[0] = homo0_1 # target image
		images_b3[0] = homo1_1 # target image
		images_b4[0] = homo1_1 # target image
		p_b1 = 0
		p_b2 = 0
		p_b3 = 0
		p_b4 = 0
		# If competitior image should be present in the block
		if folder in b1_homos:
			p_b1 = 1
			images_b1[1] = homo1_1 # set competitor
		if folder in b2_homos:
			p_b2 = 1
			images_b2[1] = homo1_1
		if folder in b3_homos:
			p_b3 = 1
			images_b3[1] = homo0_1
		if folder in b4_homos:
			p_b4 = 1
			images_b4[1] = homo0_1
		
		#for each homophone, generate combined image (the entire grid), set the reference image
		ids = list(range(0,3))+list(range(4,7))
		random.shuffle(ids)
		combined_forb1, b1_tp, b1_cp = combine_images(ids,images_b1, homo0_2)
		combined_forb2, b2_tp, b2_cp = combine_images(ids,images_b2, homo0_2)
		random.shuffle(ids)
		combined_forb3, b3_tp, b3_cp = combine_images(ids, images_b3, homo1_2)
		combined_forb4, b4_tp, b4_cp = combine_images(ids, images_b4, homo1_2)

		b1_homo_slides.append((folder+'_h0_b1_'+task+'_'+('p' if p_b1 else 'a')+'_tp'+b1_tp+'_cp'+b1_cp+'.jpeg', combined_forb1))
		b2_homo_slides.append((folder+'_h0_b2_'+task+'_'+('p' if p_b2 else 'a')+'_tp'+b2_tp+'_cp'+b2_cp+'.jpeg', combined_forb2))
		b3_homo_slides.append((folder+'_h1_b3_'+task+'_'+('p' if p_b3 else 'a')+'_tp'+b3_tp+'_cp'+b3_cp+'.jpeg', combined_forb3))
		b4_homo_slides.append((folder+'_h1_b4_'+task+'_'+('p' if p_b4 else 'a')+'_tp'+b4_tp+'_cp'+b4_cp+'.jpeg', combined_forb4))
	
	# Make filler Slides
	b1_filler_slides = []
	b2_filler_slides = []
	b3_filler_slides = []
	b4_filler_slides = []
	b_filler_slides = {1:b1_filler_slides, 2:b2_filler_slides, 3:b3_filler_slides, 4:b4_filler_slides}
	for i,b in b_filler_slides.items():
		for name in random.sample(distractor_names, 16):
			target_file = name+'1.png'
			match_file = name+'2.png'
			if task == 'Exact':
				match_file = target_file
			# target at id 4; randomly placed exact match of target; rest is random distractors
			# 3 by 3, center one in each 640/360 rectangle, random order
			target = Image.open(path+'/distractors/'+target_file)
			match = Image.open(path+'/distractors/'+match_file)
			unrelated = [Image.open(path+'/distractors/'+file+'1.png').copy() for file in distractor_names if file.split('/')[-1] != name]
			unrelated += [Image.open(path+'/distractors/'+file+'2.png').copy() for file in distractor_names if file.split('/')[-1] != name]

			# Setup the other 8 images:
			images_b = random.sample(unrelated, 6)
			images_b[0] = match # matching image (target)
			#for each homophone, generate a b1 b2 b3 b4 image (the entire grid)
			ids = list(range(0,3))+list(range(4,7))
			random.shuffle(ids)
			combined, tp, cp = combine_images(ids, images_b, target)
			b.append((name+'_h0_b'+str(i)+'_'+task+'_a'+'_tp'+tp+'_cp'+cp+'.jpeg', combined))	
		random.shuffle(b)

	block1 = b1_filler_slides[4:] + b1_homo_slides #12 + 8 = 20
	block2 = b2_filler_slides[4:] + b2_homo_slides
	block3 = b3_filler_slides[4:] + b3_homo_slides
	block4 = b4_filler_slides[4:] + b4_homo_slides

	random.shuffle(block1)
	random.shuffle(block2)
	random.shuffle(block3)
	random.shuffle(block4)

	stimuli_l1 = b1_filler_slides[:4] + block1 + block4
	save_stimuli(task, stimuli_l1, 'List1')
	stimuli_l2 = b2_filler_slides[:4] + block2 + block3
	save_stimuli(task, stimuli_l2, 'List2')
	stimuli_l3 = b4_filler_slides[:4] + block4 + block1
	save_stimuli(task, stimuli_l3, 'List3')
	stimuli_l4 = b3_filler_slides[:4] + block3 + block2
	save_stimuli(task, stimuli_l4, 'List4')

def save_stimuli(task, stimuli, list_id):
	for i in range(len(stimuli)):
		name, image = stimuli[i] 
		color = (28, 99, 206)
		width = 4
		draw = ImageDraw.Draw(image)
		x = 0
		# Blue square
		draw.rectangle([810, 390-x, 1112, 392-x], fill=color)
		draw.rectangle([810, 690-x, 1112, 692-x], fill=color)
		draw.rectangle([810, 390-x, 812, 690-x], fill=color)
		draw.rectangle([1110, 390-x, 1112, 690-x], fill=color)
		#draw text
		if task == 'Linguistic':
			font = ImageFont.truetype('./Roboto-Black.ttf', 60)			
			draw.rectangle([810, 690-20, 1112, 692+50], fill=color)
			draw.rectangle([813, 690-17, 1109, 692+47], fill=(255, 255, 255))
			label = name.split('_')[0]
			if label == 'flower':
				if name.split('_')[1] == 'h1': label = 'flour'
			size = font.getsize(label)
			draw.text((640+(640-size[0])/2,665), label, fill='black', font = font)
		folder = './Stimuli/'+task+'/'+list_id+'/'
		if not os.path.exists(folder): os.makedirs(folder)
		image.save(folder+'{:02d}'.format(i+1)+'_'+list_id+'_'+name, 'PNG')

if __name__ == "__main__":
	# Get distractor images
	w = 1920
	h = 1080
	gap = int((1080 - 3*dim)/3)
	# crop_images()
	path = './Selected_Images' 
	# distractors = [Image.open(path+'/distractors/'+file) for file in os.listdir(path+'/distractors/')]
	distractor_names = list(set([file.split('.')[0][:-1] for file in os.listdir(path+'/distractors/') if '.DS' not in file]))
	random.shuffle(distractor_names)
	distractors_1 = [Image.open(path+'/distractors/'+file+'1.png') for file in distractor_names]
	distractors_2 = [Image.open(path+'/distractors/'+file+'2.png') for file in distractor_names]
	id2pos = {
		0: (int((w-3*dim-2*gap)/2),int((1080-2*dim-gap)/2)),
		1: (int((w-3*dim-2*gap)/2),int((1080-2*dim-gap)/2)+dim+gap),
		2: (640+int((640-dim)/2),int(0*360+(360-dim)/2)),
		3: (640+int((640-dim)/2),int(1*360+(360-dim)/2)),
		4: (640+int((640-dim)/2),int(2*360+(360-dim)/2)),
		5: (int((w-3*dim-2*gap)/2)+2*(gap+dim),int((1080-2*dim-gap)/2)),
		6: (int((w-3*dim-2*gap)/2)+2*(gap+dim),int((1080-2*dim-gap)/2)+dim+gap)
		}
		# i:(int((i%3)*640+(640-dim)/2),int((math.floor(i/3)*360)+(360-dim)/2)) for i in range(7)}
	# print(id2pos)
	generate_task('Exact')
	generate_task('Semantic')
	generate_task('Linguistic')

