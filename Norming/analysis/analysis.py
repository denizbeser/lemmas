import os
import ast

participants = []

thresholds = [0.3,0.4,0.5]
image2labels2Counts = {} 
with open('results.txt', 'w') as out:
	with open('analysis-formatted.txt', 'r') as f:
		lines = f.readlines()
		for threshold in thresholds:
			out.write('Threshold: '+str(threshold) + '\n')
			i = 0
			while i < len(lines):
				line = lines[i]
				if line == '\n': i+=1; continue
				split = line.split()
				typ = split[0]
				if typ == 'Image:':
					image, count = split[1], split[2]
					count = float(count.split(':')[-1])
					image2labels2Counts[image] = {}
					i += 1
					line = lines[i]
					while line !='\n':
						label = line.split()[1]
						n = float(line.strip().split(':')[-1])
						image2labels2Counts[image][label] = n 
						i += 1
						line = lines[i]
						if float(n)/float(count) > threshold:
							print(image, label)
							out.write(image+ ' label: '+label+'\n')
				i += 1
			out.write('\n')
# print(image2labels2Counts)
