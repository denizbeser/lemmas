import os
import ast

participants = []
for file in os.listdir('./results/'):
	if '.DS' in file:continue
	with open('./results/'+file, 'r') as f:
		for line in f:
			results = {}
			if line[0] == '[':
				#data line
				line = line.replace('null','None').replace('false','False').replace('true','True').strip('\n')
				data = ast.literal_eval(line)
				ID1 = data[3][0][-1][-1]
				ID2 = data[-3][-1][-1][-1]
				if ID1 == 'A3NDOGHRT2TGEH': continue # cheated
				if ID1 == 'A31VF0URTIOPGP': continue # BS
				if ID1 == 'A3EKETMVGU2PM9': continue # BS answers; #A2SKT758ZZ91FY was bad
				if ID1 != ID2:
					print('Bad participant:', ID1, ID2)
					continue

				for i in range(2,12):
					label = data[3][3+i*5][-3][-1]
					image = data[3][4+i*5][-3][-1]
					results[image] = label.lower()
					if label.lower() == 'l n': print(ID1)
			participants.append(results)

		image2labels2Counts = {} 
		for results in participants:
			for image, label in results.items():
				if image in image2labels2Counts:
					if label in image2labels2Counts[image]:
						image2labels2Counts[image][label] = image2labels2Counts[image][label] + 1
					else:
						image2labels2Counts[image][label] = 1
				else:
					image2labels2Counts[image] = {label: 1}

# print(image2labels2Counts)
with open('analysis.txt', 'w') as f:
	for image in sorted(image2labels2Counts):
		n = sum(count for label,count in image2labels2Counts[image].items())
		f.write('Image: '+image + ' n:' + str(n) + '\n')
		for label,count in image2labels2Counts[image].items():
			f.write('Label: '+label + ' n:' + str(count) + '\n')
			if (count/n >=0.5):
				print(image, n, label, count)
		f.write('\n')



